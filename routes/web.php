<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//backend
//login
Route::get('/login','UserController@getLogin');
Route::post('/login','UserController@postLogin');

//register
Route::get('/register','UserController@getRegister');
Route::post('/register','UserController@postRegister');
//route admin
Route::get('admin/index','AdminController@index');




Route::group(['prefix' => 'admin'], function () {
    Route::group(['prefix' => 'sanpham'], function () {
        Route::get('/listProduct','ProductController@index');

        Route::get('/addProduct','ProductController@addProduct');
        Route::post('storeProduct','ProductController@store');
       
       
    });
     Route::group(['prefix' => 'pages'], function () {
        Route::get('/','ProductController@index');
       
       
    });
    Route::group(['prefix' => 'loaisp'], function () {
        Route::get('/listLoaisp','ProductTypeController@index');
       //route add
       Route::get('/addProductType','ProductTypeController@addProductType');
       Route::post('/storeProductType','ProductTypeController@store');
       
    });
});


//frontend
//route pages
Route::get('/trangchu','HomeController@index');