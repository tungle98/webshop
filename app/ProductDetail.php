<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    //
    protected $table ='product_type';
    protected $fillablle = ['name','title','image'];

    public function Product()
    {
        return $this->belongsToMany('App\Product');
    }
}

