<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductDetail;
class ProductTypeController extends Controller
{
    //
    public function index()
    {
        return view('admin.loaisp.list_loaisp');
    }

     public function addProductType()
    {
        $productType = ProductDetail::all();
        return view('admin/loaisp/add_loaisp',compact('productType'));
    }
    public function store(Request $request)
    {   
        $productdetail = new ProductDetail;
        $productdetail->name = request('name');
        $productdetail->title = request('title');
        $productdetail->image = request('image');
          $productdetail->save();
        
          return redirect('admin/loaisp/addProductType');
          ;
    }

}
