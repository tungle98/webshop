<?php

namespace App\Http\Controllers;
use Illuminate\Support\MessageBag;
use Illuminate\Http\Request;
use Redirect;
class UserController extends Controller
{
    //chuc nang dang nhap
    public function getLogin()
    {
        return view('admin.pages.login');
    }

    public function postLogin(Request $request)
    {
       $arr=[
            'email'=>$request->email,
            'password'=>$request->password
        ];

        
            return Redirect::to('admin/index');
        
    }

// chuc nang dang ki
    public function getRegister()
    {
        return view('admin.pages.register');
    }

    public function postRegister(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = hash::make('$request->password');


        $user->save();

        return redirect('register');
    }

    public function logout()
    {
        
    }
}
