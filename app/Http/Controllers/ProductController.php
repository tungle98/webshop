<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\ProductDetail;
class ProductController extends Controller
{
    //show list product
    public  function index()
    {
        $product = Product::all();
        return view('admin/sanpham/list_product',compact('product'));
    }

    //Them san pham
   
    public function addProduct()
    {
        $productType = ProductDetail::all();
        return view('admin/sanpham/add_product',compact('productType'));
    }
    public function store()
    {   
        $product = new Product;
        $product->productdetail_id = request('productdetail_id');
        $product->name = request('display_name');
        $product->title = request('title');
        $product->price = request('price');
        $product->new = request('new');
        $product->keyword = request('keyword');
        $product->image = request('image');
        $product->promotion = request('promotion');
             
          $product->save();
        
          return redirect('admin/sanpham/addProduct');
          ;
    }



}
