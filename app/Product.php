<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //
    protected $table ='products';
    protected $fillablle = ['name','title','price','new','keyword','promotion'];

    public function ProductDetail()
    {
        return $this->belongsToMany('App\ProductDetail');
    }
}
