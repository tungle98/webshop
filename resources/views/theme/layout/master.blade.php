<!DOCTYPE html>
<html lang="en">
<head>
	<title>Pacific - Free Bootstrap 4 Template by Colorlib</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Arizonia&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

	<link rel="stylesheet" href="{{url('public/front_end/css/animate.css')}}">
	
	<link rel="stylesheet" href="{{url('public/front_end/css/owl.carousel.min.css')}}">
	<link rel="stylesheet" href="{{url('public/front_end/css/owl.theme.default.min.css')}}">
	<link rel="stylesheet" href="{{url('public/front_end/css/magnific-popup.css')}}">
	
	<link rel="stylesheet" href="{{url('public/front_end/css/bootstrap-datepicker.css')}}">
	<link rel="stylesheet" href="{{url('public/front_end/css/jquery.timepicker.css')}}">

	
	<link rel="stylesheet" href="{{url('public/front_end/css/flaticon.css')}}">
	<link rel="stylesheet" href="{{url('public/front_end/css/style.css')}}">
</head>
<body>
	@include('theme.layout.header')
	<!-- END nav -->
	
	<!-- content -->
	@yield('content')
<!--footer-->
@include('theme.layout.footer')
	
			
			

			<!-- loader -->
			<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px"><circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee"/><circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00"/></svg></div>


			<script src="{{url('public/front_end/js/jquery.min.js')}}"></script>
			<script src="{{url('public/front_end/js/jquery-migrate-3.0.1.min.js')}}"></script>
			<script src="{{url('public/front_end/js/popper.min.js')}}"></script>
			<script src="{{url('public/front_end/js/bootstrap.min.js')}}"></script>
			<script src="{{url('public/front_end/js/jquery.easing.1.3.js')}}"></script>
			<script src="{{url('public/front_end/js/jquery.waypoints.min.js')}}"></script>
			<script src="{{url('public/front_end/js/jquery.stellar.min.js')}}"></script>
			<script src="{{url('public/front_end/js/owl.carousel.min.js')}}"></script>
			<script src="{{url('public/front_end/js/jquery.magnific-popup.min.js')}}"></script>
			<script src="{{url('public/front_end/js/jquery.animateNumber.min.js')}}"></script>
			<script src="{{url('public/front_end/js/bootstrap-datepicker.js')}}"></script>
			<script src="{{url('public/front_end/js/scrollax.min.js')}}"></script>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false"></script>
			<script src="{{url('public/front_end/js/google-map.js')}}"></script>
			<script src="{{url('public/front_end/js/main.js')}}"></script>
			
		</body>
		</html>