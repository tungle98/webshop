@extends('admin.layouts.master')
@section('content')
	<div class="main-container">
   		 <div class="row">
      
            <div class="col-md-6 col-sm-8">
              <form action="{{url('admin/sanpham/storeProduct')}}" method="POST" enctype="multipart/form-data">
                @csrf
              
                <div class="form-group">
                  <label for="namesp">Loai  sản phẩm</label>
                  <select>
                      <option value="">-- lựa chọn --</option>
                      @foreach($productType as $item )                                   
                        <option  value="" name="productdetail_id">{{$item->name}}</option>    
                      @endforeach
                  </select>
                </div>
                <div class="form-group">
                  <label for="namesp">Tên  sản phẩm</label>
                  <input type="text" class="form-control" placeholder="Enter name " name="name">
                </div>
                <div class="form-group">
                  <label for="pwd">Title:</label>
                  <input type="text" class="form-control" placeholder="Enter title" name="title">
                </div>
                <div class="form-group">
                    <label for="pwd">price:</label>
                    <input type="text" class="form-control" placeholder="Enter price" name="price">
                  </div>
                  <div class="form-group">
                    <label for="pwd">new:</label>
                    <input type="text" class="form-control" placeholder="Enter new" name="new">
                  </div>
                  <div class="form-group">
                    <label for="pwd">keyword:</label>
                    <input type="text" class="form-control" placeholder="Enter keyword" name="keyword">
                  </div>
                  <div class="form-group">
                    <label for="pwd">image:</label>
                    <input type="file" class="form-control" placeholder="Enter image" name="image">
                  </div>
                  <div class="form-group">
                    <label for="pwd">promotion:</label>
                    <input type="text" class="form-control" placeholder="Enter promotion" name="promotion">
                  </div>
                
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
        </div>


	</div>
@endsection