@extends('admin.layouts.master')
@section('content')
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
        <div class="min-height-200px">
            <div class="page-header">
                <div class="row">
                    <div class="col-md-6 col-sm-12">
                        <div class="title">
                            <h4>Data Product</h4>
                        </div>
                        <nav aria-label="breadcrumb" role="navigation">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">DataTable</li>
                            </ol>
                        </nav>
                    </div>
                    <div class="col-md-6 col-sm-12 text-right">
                        <div class="dropdown">
                            <a class="btn btn-primary dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                January 2020
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="#">Export List</a>
                                <a class="dropdown-item" href="#">Policies</a>
                                <a class="dropdown-item" href="#">View Assets</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-sm btn-primary btn-flat" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i>Thêm mới</button>
        </div>
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog modal-xs">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Thêm mới</h4>
                    </div>
                 <form  action="" method="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="modal-body">
                         <div class="row">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Loại sản phẩm:</label>
                                <div class="col-lg-9">
                                    <select class="form-control" name="code_name_id" required>
                                        <option value="">-- lựa chọn --</option>                                    
                                        <option  value=""></option>    
                                    </select>    
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="col-lg-2 control-label">Tên sản phẩm:</label>
                                <div class="col-lg-9">
                                    <input type="number" class="form-control" placeholder="Nhập số lượng" value="" name="" required>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button class="btn btn-primary" type="submit" name='submit'>add</button>
                       </div>
                   </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
            <!-- Simple Datatable start -->

            <div class="card-box mb-30">
                <div class="pd-20">
                    <h4 class="text-blue h4">Data Table Simple</h4>
                    <p class="mb-0">you can find more options <a class="text-primary" href="https://datatables.net/" target="_blank">Click Here</a></p>
                </div>
                <div class="pb-20">
                    <table class="data-table table stripe hover nowrap">
                        <thead>
                            <tr>
                                <th class="table-plus datatable-nosort">Loại sản phẩm</th>
                                <th>Tên sản phẩm</th>
                                <th>Title</th>
                                <th>Giá </th>
                                <th>New</th><!--Hàng mới là 1, cũ là 0-->
                               
                                <th>keyWord</th>
                                <th>Image</th>
                                <th>Giá khuyến mãi</th>
                                <th class="datatable-nosort">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($product as $item)
                            <tr>
                               
                                    <td>{{$item->productdetail_id}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->title}}</td>
                                    <td>{{$item->price}}</td>
                                    <td>{{$item->new}}</td>
                                    <td>{{$item->keyword}}</td>
                                    <td>{{$item->image}}</td>
                                    <td>{{$item->promotion}}</td>
                                    <td>
                                        <div class="dropdown">
                                            <a class="btn btn-link font-24 p-0 line-height-1 no-arrow dropdown-toggle" href="#" role="button" data-toggle="dropdown">
                                                <i class="dw dw-more"></i>
                                            </a>
                                            <div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list">
                                                <a class="dropdown-item" href="#"><i class="dw dw-eye"></i> View</a>
                                                <a class="dropdown-item" href="#"><i class="dw dw-edit2"></i> Edit</a>
                                                <a class="dropdown-item" href="#"><i class="dw dw-delete-3"></i> Delete</a>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                           
                            @endforeach
                               
                        
                        </tbody>
                    </table>
                </div>
            </div>
     
        </div>
        <div class="footer-wrap pd-20 mb-20 card-box">
            DeskApp - Bootstrap 4 Admin Template By 
        </div>
    </div>
</div>
@endsection